\section{Static Analysis Rules}

This section outlines actions when certain instructions are executed to ensure deterministic
behavior. We make the typical assumption that the program is written in a high level language and
The static analyzer is expected to be run on assembly code emitted by the compiler. It's the job
of the compiler to make sure that it emits instructions that follow the rules laid down by our
static analyzer. However, we don't cover the compiler specific changes in detail, instead
just focusing on the rules that we expect the compiler to follow. We describe the rules followed
by a pseudocode representation.

\begin{itemize}
\item {\em Consistent values in RFLAGS} Referring to the discussion in section 4, there should
  be a way for all instructions that leave the status bits in an inconsistent state to have
  deterministic values. Every call to such instruction should be followed by a call to
  POPF/POPFD/POPFQ instruction that resets known undefined bits of the flag register to zero.
  Here are some representative examples:

  {\textbf DIV} instruction
  The Div instruction leaves CF, OF, SF, ZF, AF, and PF flags in and undefined state. So, the static
  analyzer should check for clearing of all the flag bits after div is executed. (Trying to set reserved
  flags is ignored by the processor.)
  
  \begin{lstlisting}[frame=single]
    ...
    div %ebx
    pushf #the first 16 bits (EFLAGS) to the stack
    pop %ecx # Can we directly and with esp ?
    and $0, %ecx 
    push %ecx # back to stack
    popfd #first 16 bits are zero now
  \end{lstlisting}

  {\textbf MUL} instruction
  The Mul instruction sets OF and CF flags to 0 if the upper half of the result is 0 or else it sets
  them to 1. SF, ZF, AF and PF flags are undefined.
  
  \begin{lstlisting}[frame=single]
    ...
    mul %eax, %eax
    pushf #the first 16 bits (EFLAGS) to the stack
    pop %ecx # Can we directly and with esp ?
    and $0x801, %ecx 
    push %ecx # back to stack
    popfd 
  \end{lstlisting}

  For efficient scanning, the static analyzer can maintain two tables: one of instructions that leave
  status flags entirely undefined (such as DIV) and intructions that partially set status flags.
  Here's an aggregate example table that the analyzer can build:
  
  \begin{center}
  \begin{tabular}{ l | c }
    \hline
    Instruction & Mask  \\ \hline
    DIV & 0  \\ \hline
    MUL & 0x801  \\
    \hline
  \end{tabular}
  \end{center}
  
\item {\em Scanning for FPU instructions} Our static analyzer disallows using x87 FPU instructions. The
  Intel Software Developer's Manual \cite{intelmanual} lists close to a hundred x87 floating point unit
  instructions. A straight forward approach is for the static analyzer to maintain a table of such instructions
  and scan for a match as it is analyzing program code.

  Although FPU is an integral part of modern microprocessors, any sane application program is expected to
  perform some sort of FPU detection. One method is to execute cpuid(EAX=1) instruction and check bit 0 in
  the EDX register. Our sandbox can mandate the detection of fpu using the cpuid method:

    \begin{lstlisting}[frame=single]
    ...
      mov $0, %eax
      cpuid
      and $1, %edx
      cmp $1, %edx
      je havefpu
    ...
  \end{lstlisting}

  Our proposed {\em runtime fixer} is expected to trap cpuid instrutions and return a default set of
  values including clearing the bit 0 for cpuid(EAX=1). However, until that block is in place, our
  static analyzer maintains a table of x87 instructions and fails the program run on detecting one.

\item {\em Redundant instruction prefixes} As discussed in Section 4, our static analyzer checks for
  redundant instruction prefixes. Instruction prefix 0x66 determines the operand size. For example,
  MOV AX, [ADDR] is encoded the same as MOV EAX, [ADDR] but with the 0x66 intruction prefix that changes
  the operand size to 32 bit.

  For byte-size instructions such as POPA, the instruction prefix is redundant since this instruction
  does not take any operands. Our static analyzer aims to scan at the disassembly level, meaning, we expect
  to feed the output of ``gcc -S'' to our static analyzer. Usually, any sane compiler will avoid using
  instruction prefixes with such instructions. Nevertheless, our analyzer should check for the following
  and flag an error:

  \begin{lstlisting}[frame=single]
    ...
    .byte 0x66
    popa
    ...
  \end{lstlisting}

  XXX What about other prefixes such as REX ?

\item {\em Reliable Disassembly}
  Reliable disassembly ensures that all instructions that can be reached during program flow are identified
  during the disassembly phase. This ensures that our static analyzer scans through all paths that the program
  can reach. Our code assumes that the compiler emits code that satisfies the above rule. However, in practice
  that might not be true since optimized code generation is the norm for modern compilers.

  To satisy the above criteria, the static analyzer checks for two things \cite{nacl}:
  Indirect jumps/branch/call instructions must always use register addressing mode and
  Every indirect call should always be preceeded by an AND instruction that ensures that the address is aligned
  and cannot cross the boundary.

  \begin{lstlisting}[frame=single]
    ...
    .code32
    and %eax, 0xffffffe0
    jmp *%eax
    ...
    .code64
    ...
  \end{lstlisting}

\item {\em Disallowed Instructions}
  To reduce unintended behavior, the static analyzer checks for certain instructions and flags an
  error irrespective of the way they are used. Privileged instructions such as WRMSR, MOV to CR3,
  VMX class of instructions are disallowed. Also disallowed are instructions that are rarely used such as XLAT
  (Translation table lookup instruction) and Binary Coded Arithmetic instructions (AAD, AAM, AAA) since
  they are seldom used with modern x86 processors.
  
\end{itemize}

  
