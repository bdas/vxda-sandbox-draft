# Simple standalone and configuration-free makefile for this document...

OUT = main

all: $(OUT).ps $(OUT).pdf

$(OUT).dvi: *.tex
	latex $*

$(OUT).ps: $(OUT).dvi
	dvips -P cmz -t letter -o $@ $<

$(OUT).pdf: $(OUT).ps
	ps2pdf $<


# Make a first-cut HTML version (which then requires manual tweaks!)
html:
	latex2html -split 0 -show_section_numbers -local_icons \
		-noinfo -nonavigation $(OUT).tex 

clean:
	rm -f *.aux *.bbl *.blg *.dvi *.log *.pdf *.ps

